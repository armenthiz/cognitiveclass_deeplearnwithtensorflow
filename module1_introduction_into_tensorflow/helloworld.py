import tensorflow as tf

a = tf.constant([2])
b = tf.constant([3])

c = tf.add(a, b)
#c = a + b

session = tf.Session()

with tf.Session() as sess:
    result = sess.run(c)
    print(result)

