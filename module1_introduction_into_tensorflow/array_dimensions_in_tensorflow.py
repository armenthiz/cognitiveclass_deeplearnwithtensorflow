import tensorflow as tf

Scalar = tf.constant([2])
Vector = tf.constant([5,6,2])
Matrix = tf.constant([[1,2,3], [4,5,6], [7,8,9]])
Tensor = tf.constant([ [[1,2,3],[4,5,6]], [[7,8,9],[4,5,6]], [[1,2,3],[4,5,6]] ])

with tf.Session() as sess:
    result = sess.run(Scalar)
    print "Scalar (1 entry): \n %s \n" % result
    result = sess.run(Vector)
    print "Vector (3 entry): \n %s \n" % result
    result = sess.run(Matrix)
    print "Matrix (3x3 entry): \n %s \n" % result
    result = sess.run(Tensor)
    print "Tensor (3x3x3 entry): \n %s \n" %result
