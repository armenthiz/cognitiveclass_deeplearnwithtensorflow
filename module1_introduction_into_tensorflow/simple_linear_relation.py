# import required packages
import numpy as np
import tensorflow as tf
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
#%matplotlib inline
plt.rcParams['figure.figsize'] = (10,6)

# define independent variable
X = np.arange(0.0, 5.0, 0.1)

# You can adjust the slope and intercept to verify the changes
a = 2
b = 0
Y = a*X + b

plt.plot(X,Y)
plt.ylabel('Dependent Variable')
plt.xlabel('Independent Variable')
plt.show()
