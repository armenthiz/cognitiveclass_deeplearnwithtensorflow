import tensorflow as tf

# List of Datatype will be used in tf op
# Data Type      | Python Type   | Description
# DT_FLOAT       | tf.float32    | 32 bits floating point
# DT_DOUBLE      | tf.float64    | 64 bits floating point
# DT_INT8        | tf.int8       | 8 bits signed integer
# DT_INT16       | tf.int16      | 16 bits signed integer
# DT_INT32       | tf.int32      | 32 bits signed integer
# DT_INT64       | tf.int64      | 64 bits signed integer
# DT_UINT8       | tf.uint8      | 8 bits unsigned integer
# DT_STRING      | tf.string     | Variable length byte arrays, Each elements of a tensor is byte of array
# DT_BOOL        | tf.bool       | Boolean
# DT_COMPLEX64   | tf.complex64  | Complex number made of two 32 bits floating points, real and imaginary parts
# DT_COMPLEX128  | tf.complex128 | Complex number made of two 64 bits floating points, real and imaginary parts
# DT_QINT8       | tf.qint8      | 8 bits signed integer used in quantized Ops
# DT_QINT32      | tf.qint32     | 32 bits signed integer used in quantized Ops
# DT_QUINT8      | tf.quint8     | 8 bits unsigned integer used in quantized Ops

a = tf.placeholder(tf.float32)

b = a * 2

# To pass data to model (placeholder) using feed_dict to pass a dict to each placeholder name
with tf.Session() as sess:
    result = sess.run(b, feed_dict={a:3.5})
    print(result)
    
    # And since data in TensorFlow passed in form of multidimensional arrays, we can pass any kind of tensor through the placeholders to get the answers of the simple multiplication operation:
    dictionary = { a: [ [ [1,2,3], [2,3,4], [4,5,6] ], [ [6,7,8], [8,9,0], [0,1,2] ] ] }
    result = sess.run(b, feed_dict=dictionary)
    print(result)
