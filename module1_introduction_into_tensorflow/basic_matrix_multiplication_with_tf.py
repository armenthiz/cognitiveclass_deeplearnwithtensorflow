import tensorflow as tf

Matrix_one = tf.constant([[2,3], [3,4]])
Matrix_two = tf.constant([[2,3], [3,4]])

first_operation = tf.matmul(Matrix_one, Matrix_two)

with tf.Session() as session:
    result = sess.run(first_operation)
    print "Defined using tensorflow function"
    print(result)

