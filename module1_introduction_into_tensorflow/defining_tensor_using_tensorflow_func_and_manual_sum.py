import tensorflow as tf

Matrix_one = tf.constant([[1,2,3], [4,5,6], [7,8,9]])
Matrix_two = tf.constant([[7,8,9], [4,5,6], [1,2,3]])

first_operation = tf.add(Matrix_one, Matrix_two)
second_operation = Matrix_one + Matrix_two

with tf.Session() as sess:
    result = sess.run(first_operation)
    print "Defined using tensorflow function :"
    print(result)
    result = sess.run(second_operation)
    print "Defined using normal expressions: "
    print(result)

